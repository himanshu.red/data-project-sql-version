-- Queries for creating table for matches.csv

CREATE TABLE matches 
(
	id BIGSERIAL PRIMARY KEY NOT NULL, 
	season TEXT, 
	city TEXT, 
	DATE TEXT, 
	team1 TEXT, 
	team2 TEXT, 
	toss_winner TEXT, 
	toss_decision TEXT, 
	result TEXT, 
	dl_applied INTEGER, 
	winner TEXT, 
	win_by_run INTEGER, 
	win_by_wicket INTEGER, 
	player_of_match TEXT, 
	venue TEXT,  
	umpire1 TEXT, 
	umpire2 TEXT, 
	umpire3 TEXT
);

-- Query for creating table for  deliveries.csv

CREATE TABLE deliveries 
(
	match_id BIGSERIAL NOT NULL, 
	inning INTEGER, 
	batting_team TEXT, 
	bowling_team TEXT,
	over INTEGER, 
	ball INTEGER, 
	batsman TEXT, 
	non_striker TEXT, 
	bowler TEXT, 
	is_super_over TEXT, 
	wide_runs INTEGER, 
	bye_runs INTEGER, 
	legbye_runs INTEGER, 
	noball_runs INTEGER, 
	penalty_runs INTEGER, 
	basman_runs INTEGER,
	extra_runs INTEGER, 
	total_runs INTEGER,
	player_dismissed TEXT,
	dismissal_kind TEXT, 
	fielder TEXT
);

-- 1. Number of matches played per year of all the years in IPL.  

SELECT DISTINCT season, COUNT (season) AS matches 
FROM matches 
GROUP BY season 
ORDER BY season;

-- 2.  Number of matches won of all teams over all the years of IPL.  

SELECT DISTINCT winner, COUNT(winner) AS wins 
FROM matches 
GROUP BY winner 
HAVING winner IS NOT NULL 
ORDER BY wins DESC;

-- 3. For the year 2016 get the extra runs conceded per team.  

SELECT  DISTINCT deliveries.bowling_team, SUM (deliveries.extra_runs) as extra_runs 
FROM deliveries
LEFT JOIN matches ON matches.id = deliveries.match_id
WHERE matches.season = '2016'
GROUP BY deliveries.bowling_team ;

-- 4.  For the year 2015 get the top economical bowlers.  

SELECT DISTINCT deliveries.bowler, (SUM (deliveries.total_runs) / (COUNT(deliveries.bowler)/6)) as ECONOMY
FROM deliveries
LEFT JOIN matches ON deliveries.match_id = matches.id
WHERE matches.season = '2015'
GROUP BY deliveries.bowler
ORDER BY economy ASC
LIMIT 1 OFFSET 1;