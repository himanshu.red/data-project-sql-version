# **Data project - postgreSQL Version**
## Applications Required
1. [PostgreSQL](https://www.postgresql.org/download/)
2. [pgAdmin4](https://www.pgadmin.org/download/)


## Steps to follow 
1. Open the terminal. 
2. Run command `sudo -i -u postgres`  to switch to the postgres account running on your local server.
3. Run command `psql` to run the postgres prompt. 
4. To see all the databases running on your local server run this command `\l`
5. Create a separate database for this project. To create the database run this command : `CREATE DATABASE ipl_project`
6. To connect to this database, run the following command : `\c ipl_project`.
7. Not let's create two schemas for **matches.csv** and **deliveries.csv**. Query to create schemas for both, the query has been given in the text file. 
8. To add the data to the tables from .csv file we'll use pgAdmin4. So let's open pgAdmin4(the GUI agent from postgreSQL); 
9.  On the upper left corner click on servers -> \<the local server on which ipl_project database resides> -> ipl_project -> schemas -> public -> tables.
10. Right click on the tables -> click on the import/export Data -> choose the file name, formate of the file -> click on OK. Do this for both the files. 
11. Now we have added the data to the both the tables. Let's run the queries on the postgres command prompt. Repeat the steps 1, 2, 3 & 6 and run the queries.